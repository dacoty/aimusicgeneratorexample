# AIMusicGenerator Example


## Requirements
 - Docker on your computer ( I used docker desktop)
 - Go to https://huggingface.co/facebook/musicgen-small to see the code i used in the python script (look at "transformers usage")
 - NOTE: you don't need a huggingface account to run this! Also, if you prefer to run locally just install python and the pip packages listed in the dockerfile locally, then copy and run the python script. this will work the same. I just opted to put in docker to keep things independent of my local computer and keep things a little cleaner. I can easily just destroy the container when i'm done messing with it.

## Getting started

- This was based off the hugging face example here: https://huggingface.co/facebook/musicgen-small
- i just minimally dockerized it and got it to run. 

- USAGE:
  - Clone this repo and CD into the AIMusicGenExample Directory
  - `docker build -t hugging-face-music-generator . `
  - `docker run -it hugging-face-music-generator bash`
  - inside the container, do `python gen_music.py`
  - `ls` when complete in the app directory, you will see a musicgen_out.wav file
  - in a separate terminal window outside of docker, docker ps -a and grab the container ID
  - `docker cp <containerid>:/app/musicgen_out.wav <your local destination>music.wav`
  - play the file. it will be short but you will hear music

### This is just a starting point for using hugging face to create AI generated music. the gen_music.py file can be modified how you wish. 
you can do various things to suit your needs. i would recommend you look at the huggingface.co webpage and experiment and use this dockerfile as a 
starting point. Other things you can do include adding an API key and make a simple web interface, or adjust to get sounds, etc. 

Want something better and more geared towards other things? check out https://github.com/gphorvath/music-generation
( not affiliated/endorsed or anything, i just like the tool)

# notes
- be careful when setting max_new_tokens. the app( or container) may crash if this is set too high. keep it to less than 1024 if you don't make code changes. this seems to produce about a 20 second music clip
- feel fee to mess around with the other inputs in the gen_music.py and see what you can produce!
